package com.evgeny.goncharov.coreapi.mediators

import androidx.fragment.app.FragmentManager

interface SplashScreenMediator {

    fun showSplashScreen(supportFragmentManager: FragmentManager)
}