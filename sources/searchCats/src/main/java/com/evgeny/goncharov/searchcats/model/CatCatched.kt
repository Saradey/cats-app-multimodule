package com.evgeny.goncharov.searchcats.model

data class CatCatched(
    val catName: String,
    val catId: String
)