package com.evgeny.goncharov.settings.models

data class ThemeModel(
    val themeValue: Int
)